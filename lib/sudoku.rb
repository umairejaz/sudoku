module Sudoku
  VERSION = "0.3".freeze unless const_defined?(:VERSION)
end

require "sudoku/board"
require "sudoku/puzzle"

require "sudoku/printer"
